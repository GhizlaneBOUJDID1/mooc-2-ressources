**Thématique :** Traitement de texte

**Notions liées :** le logiciel Ms word

**Résumé de l’activité :** Activité sous forme d'un challenge (En binôme) le meilleur travail va l'imprimer et coller dans la revue murale de la classe.

**Objectifs :** Découverte des bases de données relationnelles. Introduire le modèle relationnel sans trop de formalisation : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel. 

**Auteur :** BOUJDID Ghizlane

**Durée de l’activité :** 2h 

**Forme de participation :** En binôme

**Matériel nécessaire :** Les ordinateurs.

**Préparation :** Aucune


**Fiche élève activité :** Disponible [ici](https://gitlab.com/GhizlaneBOUJDID1/mooc-2-ressources/-/tree/main/Ressources%20traitement%20de%20texte)   

